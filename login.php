<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: /");
    exit;
}

require_once 'lib/GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php';

$secret = "23DIKAUTXHFUHARZ";

if (isset($_POST['code'])) {
    $code = $_POST['code'];

    $ga = new PHPGangsta_GoogleAuthenticator();
    $result = $ga->verifyCode($secret, $code, 2); // 2 = 2*30sec clock tolerance

    echo $result;

}