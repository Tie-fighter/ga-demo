<?php

require_once 'lib/GoogleAuthenticator/PHPGangsta/GoogleAuthenticator.php';

$websiteTitle = 'IMANGoogleAuthenticator';

$ga = new PHPGangsta_GoogleAuthenticator();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/register.css">
</head>

<body>
<div class="newsletter-subscribe">
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Register</h2>
            <p class="text-center">
                <!--                    Secret is: --><?php //echo $ga->createSecret() . '<br>'; ?>
                Secret is: <?php echo $secret = '23DIKAUTXHFUHARZ'; ?>
            </p>
            <p class="text-center color-blue">
                Verifying Code: <strong><?php echo $ga->getCode($secret); ?></strong>
            </p>
        </div>
        <div>
            <h4 class="text-center">Google Charts URL QR-Code:</h4>
            <p class="text-center">
                <img src="<?php echo $qrCodeUrl = $ga->getQRCodeGoogleUrl($websiteTitle, $secret); ?>"
                     alt="QR CodeGoogle Url"/>
            </p>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script>
    setTimeout(function () {
        window.location.reload(1);
    }, 60000);
</script>
</body>

</html>