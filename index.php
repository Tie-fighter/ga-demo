<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IMAN Google Authenticator</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/login.css">
</head>

<body>
<div class="login-dark">
    <div class="loginform">
        <h2 class="sr-only">Login Form</h2>
        <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
        <div class="form-group"><input class="form-control" type="email" name="email" value="user1@test.local"
                                       placeholder="Email"></div>
        <div class="form-group"><input class="form-control" type="password" name="password" value="password"
                                       placeholder="Password"></div>
        <div class="form-group">
            <input class="form-control"
                   type="text"
                   id="google-code"
                   name="mfa-code"
                   placeholder="MFA Code">
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block"
                    type="submit"
                    id="submit-google-code">Log In
            </button>
        </div>
        <div class="text-center">Status:</div>
        <div id="login-status"
             class="forgot">
            Not logged in
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $('#submit-google-code').on('click', function () {
        let googlecode = $('#google-code').val();
        console.log(googlecode);
        if ($.trim(googlecode) !== '') {

            $.post('login.php', {code: googlecode}, function (data) {

                if (data === '1') {
                    $('#login-status').text('Logged in');
                    // $('div#loginform').hide();
                } else {
                    $('#login-status').text('Login failed');

                }
            });
        } else {
            $('#login-status').text('Not logged in');
        }
    });
</script>
</body>

</html>